<?php
/**
 * BLOCK: Paragfraf-block
 *
 * Gutenberg Custom Paragfraf Block
 *
 * @package `wp-blocks`: Includes block type registration and related functions.
 * @package `wp-element`: Includes the WordPress Element abstraction for describing the structure of your blocks.
 * @package `wp-i18n`: To internationalize the block's text.
 *
 * @since v0.1
 */

add_action( 'init', 'cimpleo_gutenberg_paragraf_block' );
function cimpleo_gutenberg_paragraf_block() {

	// Scripts.
	wp_register_script(
		'cimpleo-paragraf-block',
		plugins_url( 'block.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-i18n' )
	);

	register_block_type( 'cimpleo/paragraf-block', array(
		'editor_script' => 'cimpleo-paragraf-block',
	) );

}

( function( editor, components, i18n, element ) {
	var el = element.createElement;
	var registerBlockType = wp.blocks.registerBlockType;
	var RichText = wp.editor.RichText;
	var BlockControls = wp.editor.BlockControls;
	var AlignmentToolbar = wp.editor.AlignmentToolbar;
	var InspectorControls = wp.editor.InspectorControls;
	var TextControl = wp.components.TextControl;

	registerBlockType( 'cimpleo/paragraf-block', { // The name of our block. Must be a string with prefix. Example: my-plugin/my-custom-block.
		title: i18n.__( 'Block Paragraph' ), // The title of our block.
		description: i18n.__( 'A custom block for displaying paragraf.' ), // The description of our block.
		icon: 'tablet', // Dashicon icon for our block. Custom icons can be added using inline SVGs.
		category: 'common', // The category of the block.
		attributes: { // Necessary for saving block content.
			paragraf: {
				type: 'array',
				source: 'children',
				selector: 'p',
			},
			alignment: {
				type: 'string',
				default: 'left',
			},
		},

		edit: function( props ) {

			var attributes = props.attributes;
			var alignment = props.attributes.alignment;

			function onChangeAlignment( newAlignment ) {
				props.setAttributes( { alignment: newAlignment } );
			}

			return [
				el( BlockControls, { key: 'controls' }, // Display controls when the block is clicked on.
					el( AlignmentToolbar, {
						value: alignment,
						onChange: onChangeAlignment,
					} )
				),
				el( 'div', { 
					className: props.className, 
					style: { textAlign: alignment } 
				},
					el( RichText, {
						tagName: 'p',
						placeholder: i18n.__( 'Write a paragraf'),
						keepPlaceholderOnFocus: true,
						value: attributes.paragraf,
						onChange: function( newParagraf ) {
							props.setAttributes( { paragraf: newParagraf } );
						},
					}),
				)
			];
		},

		save: function( props ) {
			var attributes = props.attributes;
			var alignment = props.attributes.alignment;

			return (
				el( 'div', {
					className: props.className,
					style: { textAlign: attributes.alignment }
				},
					el( RichText.Content, {
						tagName: 'p',
						value: attributes.paragraf
					} ),
				)
			);
		},
	} );

} )(
	window.wp.editor,
	window.wp.components,
	window.wp.i18n,
	window.wp.element,
);

<?php
/**
 *
 * Plugin Name: Gutenberg Paragraf Block
 * Description: Block with paragraf which wrap 'p' tag in 'div'
 * Author: Cimpleo@VladLesovskiy
 * Author URI: https://cimpleo.com/
 * Version: 0.1
 * License: GPLv3
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Get path to current plugin.
 * @return string
 */
function get_cgpb_plugin_dir() {
	return untrailingslashit( plugin_dir_path( __FILE__ ) );
}

/**
 * Include blocks. 
 */
include_once( get_cgpb_plugin_dir() . '/blocks/paragraf/index.php' );
